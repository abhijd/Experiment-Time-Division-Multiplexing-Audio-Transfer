clc
clear all

TDM_mux2      % calling an external script TDM_mux2.m to include the function TDM_mux2
demux_TDM2    % calling an external script demux_TDM2.m to include the function demux_TDM2


%Loading 3 sample audio files
N=3;
[u1 fs1 b1]=auread('audio1.au');
[u2 fs2 b2]=auread('audio2.au');
[u3 fs3 b3]=auread('audio3.au');

ts=1/fs1;
Ts = ts; % Slot time interval
ns = Ts/ts;

disp('Press ENTER to hear the audio signals before conditioning');
pause
sound(u1)
disp('Press ENTER to hear the audio signals before conditioning');
pause
sound(u2)
disp('Press ENTER to hear the audio signals before conditioning');
pause
sound(u3)


%      Adding NOISE to emulate real world interference during transmission


for s=1:N    %TO find LENGTH
    eval(['zz'  '=u' num2str(s)])
    eval(['l' num2str(s)  '=length(zz)'])
end

mm=0;

for s=1:N    % FOR MAX LENGTH
    if eval(['l' num2str(s)])>mm
        mm=eval(['l' num2str(s)]);
    end
    
end
m=mm;

for s=1:N    %TO MAKE EQUAL LENGTH

    if eval(['l' num2str(s) ' <m;'])
        eval(['ll' '=l' num2str(s) ])

        eval(['zz' '=u' num2str(s)  ])
        eval(['uu' num2str(s)  '=zz;'])   
        for i=ll+1:m
            eval(['uu' num2str(s)  '(i)=0;'])
        end
    else
        eval(['zz' '=u' num2str(s)  ])
        eval(['uu' num2str(s)  '=zz;']) 
    end
end
clc

disp('Press ENTER to hear the audio signals before conditioning but with noise');
pause
sound(uu1)
disp('Press ENTER to hear the audio signals before conditioning but with noise');
pause
sound(uu2)
disp('Press ENTER to hear the audio signals before conditioning but with noise');
pause
sound(uu3)

% Serialize the signals
for b=1:N
    eval(['u(b,:)=uu' num2str(b) ';' ])
end 


uuu=TDM_mux2(u,ns);    % Function used to multiplex the three signals

disp('Hit [Enter}]to listen to the muxed sound');
pause
sound(uuu)

y=demux_TDM2(uuu,N,ns);   % Function used to de-multiplex the multiplexed signal


