# Multiplexing (TDM) audio transfer

A Matlab based project done during bachelor degree study to emulate Time-Division-Multiplexing and De-Multiplexing of audio files. This type of multiplexing is mainly used in telecommunication to combines multiple digital or analogue signals into one signal stream which then can be 
transmitted through a medium.

## Getting Started

Please make sure you have installed all the software listed in the Prerequisites section and then follow instructions in 'How-to-run' section to run the experiment. 

### Prerequisites

 * Matlab 2007b ( Only tested with this version. May be compatible with later versions too.)


### How-to-run

 * Clone the repository in your local machine
 * Add three au format audio files in the same directory as the project root folder
 * Either name the three audio files as audio1.au, audio2.au and audio3.au or change lines 10, 11 and 12 of sig_read_cond_TDM.m in accordance to the name of the audio files.
 * Start Matlab. Open the m file sig_read_cond_TDM.m and run it. Voila!
 
 
## Author

* **[Abhijit Das](https://www.linkedin.com/in/abhijit-das-jd/)**


## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* Hats off and gratitude to anyone whose code I may have used as reference when writing this program years ago as an infant programmer. I apologise for not remembering all references. If you ever come and claim, I will buy you a beer ot coffee whichever suits you better. 
